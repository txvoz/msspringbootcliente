/**
 * 
 */
package co.com.pragma.ereservation.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

/**
 * @author gustavo.rodriguez
 *
 */
@Data
@Entity
@Table(name = "reserva")
public class Reserva {
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String redId;
	@Temporal(TemporalType.DATE)
	private Date resFechaIngreso;
	@Temporal(TemporalType.DATE)
	private Date resFechaSalida;
	private int resCantidadPersonas;
	private String redDescripcion;
	
	@ManyToOne
	@JoinColumn(name = "cliId")
	private Cliente cliente;
	
	public Reserva() {
		// TODO Auto-generated constructor stub
	}
}
