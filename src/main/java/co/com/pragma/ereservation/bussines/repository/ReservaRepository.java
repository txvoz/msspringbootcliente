/**
 * 
 */
package co.com.pragma.ereservation.bussines.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import co.com.pragma.ereservation.model.Reserva;

/**
 * @author gustavo.rodriguez
 *
 */
public interface ReservaRepository extends JpaRepository<Reserva, String>{

	@Query("Select r from Reserva r where r.resFechaIngreso=:fechaInicio and r.resFechaSalida=:fechaSalida")
	public List<Reserva> find(@Param("fechaInicio") Date fechaIngreso,@Param("fechaSalida") Date fechaSalida);
	
}
