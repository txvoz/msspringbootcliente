/**
 * 
 */
package co.com.pragma.ereservation.bussines.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.pragma.ereservation.model.Cliente;

/**
 * @author gustavo.rodriguez
 *
 */
public interface ClienteRepository extends JpaRepository<Cliente, String> {
	
	/**
	 * Definicion de metodo para buscar clientes por el valor del apellido
	 * @param cliApellido
	 * @return
	 */
	public List<Cliente> findByCliApellido(String cliApellido);
	
	public Cliente findByIdentificacion(String cliIdentificacion);
}
