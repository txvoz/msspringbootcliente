/**

 * 
 */
package co.com.pragma.ereservation.views.resources;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.pragma.ereservation.bussines.services.ClienteService;
import co.com.pragma.ereservation.model.Cliente;
import co.com.pragma.ereservation.views.resources.vo.ClienteVO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Clase para la exposicion de los servicios web del cliente
 * @author gustavo.rodriguez
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/api/cliente")
public class ClienteResource {
	
	private final ClienteService clienteService;
	
	public ClienteResource(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	
	@PostMapping
	@ApiOperation( value = "Crear cliente", notes = "Servicio para crear un nuevo cliente")
	@ApiResponses(value = {
			@ApiResponse(code=201, message = "Cliente creado correctamente"),
			@ApiResponse(code=400, message = "Solicitud no valida")
	})
	public ResponseEntity<Cliente> createCliente(@RequestBody ClienteVO clienteVO){
		Cliente cliente = new Cliente();
		cliente.setCliNombre(clienteVO.getCliNombre());
		cliente.setCliApellido(clienteVO.getCliApellido());
		cliente.setCliIdentificacion(clienteVO.getCliIdentificacion());
		cliente.setCliDireccion(clienteVO.getCliDireccion());
		cliente.setCliTelefono(clienteVO.getCliTelefono());
		cliente.setCliEmail(clienteVO.getCliEmail());
		return new ResponseEntity<>(this.clienteService.createCliente(cliente),HttpStatus.CREATED);
	}
	
	@PutMapping("/{identificacion}")
	@ApiOperation( value = "Actualizar cliente", notes = "Servicio para actualizar un cliente")
	@ApiResponses(value = {
			@ApiResponse(code=201, message = "Cliente actualizado correctamente"),
			@ApiResponse(code=404, message = "Cliente no encontrado")
	})
	public ResponseEntity<Cliente> createCliente(@PathVariable("identificacion") String identificacion,@RequestBody ClienteVO clienteVO){
		Cliente cliente = this.clienteService.findByIdentificacion(identificacion);
		if(cliente==null) {
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		}
		cliente.setCliNombre(clienteVO.getCliNombre());
		cliente.setCliApellido(clienteVO.getCliApellido());
		cliente.setCliIdentificacion(clienteVO.getCliIdentificacion());
		cliente.setCliDireccion(clienteVO.getCliDireccion());
		cliente.setCliTelefono(clienteVO.getCliTelefono());
		cliente.setCliEmail(clienteVO.getCliEmail());
		return new ResponseEntity<>(this.clienteService.updateCliente(cliente),HttpStatus.OK);
	}
	
	@DeleteMapping("/{identificacion}")
	@ApiOperation( value = "Eliminar cliente", notes = "Servicio para eliminar un cliente")
	@ApiResponses(value = {
			@ApiResponse(code=201, message = "Cliente eliminado correctamente"),
			@ApiResponse(code=404, message = "Cliente no encontrado")
	})
	public void removeCliente(@PathVariable("identificacion") String identificacion){
		Cliente cliente = this.clienteService.findByIdentificacion(identificacion);
		if(cliente!=null) {
			this.clienteService.deleteCliente(cliente);
		}
	}
	
	@GetMapping
	@ApiOperation( value = "Listar clientes", notes = "Servicio para listar todos los clientes")
	@ApiResponses(value = {
			@ApiResponse(code=201, message = "Clientes encontrados"),
			@ApiResponse(code=404, message = "Clientes no encontrados")
	})
	public ResponseEntity<List<Cliente>> findAll(){
		return ResponseEntity.ok(this.clienteService.findAll());
	}
	
}
