/**
 * 
 */
package co.com.pragma.ereservation.views.resources.vo;

import lombok.Data;

/**
 * @author gustavo.rodriguez
 *
 */
@Data
public class ClienteVO {
	private String cliId;
	private String cliNombre;
	private String cliApellido;
	private String cliIdentificacion;
	private String cliDireccion;
	private String cliTelefono;
	private String cliEmail;
	
	public ClienteVO() {}
}
