Configuracion tarea run en eclipse:

-Run maven: clean install spring-boot:run
-Run docker: clean install docker:build

Ejecutar el siguiente comando docker para crear el contenedor y setear el dns o ip del 
servidor de base de datos. Tener en cuenta que el proyecto esta configurado para 
trabajar con una base de datos PostgreSQL.

-Comando Docker: docker run -d --name [nombre_contenedor] --add-host=postgres_server:[ip/dns] -p 8080:8080 [nombre_imagen]:latest

Tener en cuenta que este proyecto tiene configurado el POM para que se pueda generar una imagen de docker